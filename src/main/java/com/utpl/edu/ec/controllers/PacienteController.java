package com.utpl.edu.ec.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.utpl.edu.ec.models.entity.Paciente;
import com.utpl.edu.ec.models.services.IPacienteService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PacienteController {
	
	@Autowired
	private IPacienteService pacienteService;
	
	@GetMapping("/pacientes")
	public List<Paciente> index() {
		return pacienteService.findAll();
	}
	
	@GetMapping("/pacientes/page/{page}")
	public Page<Paciente> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return pacienteService.findAll(pageable);
	}
	
	@GetMapping("/pacientes/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Paciente paciente = null;
		try {
			paciente = pacienteService.findById(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (paciente == null) {
			response.put("mensaje", "El Paciente ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
	
	
	@PostMapping("/pacientes")
	public ResponseEntity<?> create(@Valid @RequestBody Paciente paciente, BindingResult result) {

		Paciente nuevoPaciente = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			nuevoPaciente = pacienteService.save(paciente);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al insertar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El paciente ha sido creado con exito!");
		response.put("especialidad", nuevoPaciente);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/pacientes/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Paciente paciente, BindingResult result, @PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Paciente pacienteActual = pacienteService.findById(id);
		Paciente pacienteActualizado = null;
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (pacienteActual == null) {
			response.put("mensaje", "Error, no se pudo editar el paciente ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			pacienteActual.setCedula(paciente.getCedula());
			pacienteActual.setNombres(paciente.getNombres());
			pacienteActual.setApellidos(paciente.getApellidos());
			pacienteActual.setSexo(paciente.getSexo());
			pacienteActual.setTelefono(paciente.getTelefono());
			pacienteActual.setUsuario(paciente.getUsuario());
			pacienteActual.setPassword(paciente.getPassword());
			
			pacienteActualizado = pacienteService.save(pacienteActual);

		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al actualizar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El paciente ha sido actualizado con exito!");
		response.put("paciente", pacienteActualizado);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("pacientes/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			pacienteService.delete(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al eliminar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El paciente ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
