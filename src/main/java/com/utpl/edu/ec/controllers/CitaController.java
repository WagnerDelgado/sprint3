package com.utpl.edu.ec.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.utpl.edu.ec.models.entity.Cita;
import com.utpl.edu.ec.models.services.ICitaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CitaController {
	
	@Autowired
	private ICitaService citaService;
	
	@GetMapping("/citas")
	public List<Cita> index() {
		return citaService.findAll();
	}
	
	@GetMapping("/citas/page/{page}")
	public Page<Cita> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return citaService.findAll(pageable);
	}
	
	@GetMapping("/citas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Cita cita = null;
		try {
			cita = citaService.findById(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (cita == null) {
			response.put("mensaje", "La Cita ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Cita>(cita, HttpStatus.OK);
	}
	
	
	@PostMapping("/citas")
	public ResponseEntity<?> create(@Valid @RequestBody Cita cita, BindingResult result) {

		Cita nuevaCita = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			nuevaCita = citaService.save(cita);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al insertar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La cita ha sido creada con exito!");
		response.put("cita", nuevaCita);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/citas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Cita cita, BindingResult result, @PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Cita citaActual = citaService.findById(id);
		Cita citaActualizada = null;
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (citaActual == null) {
			response.put("mensaje", "Error, no se pudo editar el horario ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			citaActual.setFecha_cita(cita.getFecha_cita());
			citaActual.setHora_cita(cita.getHora_cita());
			citaActual.setMedico(cita.getMedico());
			citaActual.setPaciente(cita.getPaciente());		
			citaActualizada = citaService.save(citaActual);

		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al actualizar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La cita ha sido actualizada con exito!");
		response.put("cita", citaActualizada);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("citas/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			citaService.delete(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al eliminar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La cita ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
