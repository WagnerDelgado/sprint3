package com.utpl.edu.ec.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.utpl.edu.ec.models.entity.Especialidad;
import com.utpl.edu.ec.models.services.IEspecialidadService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class EspecialidadController {
	
	@Autowired
	private IEspecialidadService especialidadService;
	
	@GetMapping("/especialidades")
	public List<Especialidad> index() {
		return especialidadService.findAll();
	}
	
	@GetMapping("/especialidades/page/{page}")
	public Page<Especialidad> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return especialidadService.findAll(pageable);
	}
	
	@GetMapping("/especialidades/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Especialidad especialidad = null;
		try {
			especialidad = especialidadService.findById(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (especialidad == null) {
			response.put("mensaje", "La Especialidad ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Especialidad>(especialidad, HttpStatus.OK);
	}
	
	
	@PostMapping("/especialidades")
	public ResponseEntity<?> create(@Valid @RequestBody Especialidad especialidad, BindingResult result) {

		Especialidad nuevaEspecialidad = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			nuevaEspecialidad = especialidadService.save(especialidad);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al insertar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La especialidad ha sido creado con exito!");
		response.put("especialidad", nuevaEspecialidad);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/especialidades/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Especialidad especialidad, BindingResult result, @PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Especialidad especialidadActual = especialidadService.findById(id);
		Especialidad especialidadActualizada = null;
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (especialidadActual == null) {
			response.put("mensaje", "Error, no se pudo editar la especialidad ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			especialidadActual.setNombre(especialidad.getNombre());
			especialidadActualizada = especialidadService.save(especialidadActual);

		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al actualizar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La especialidad ha sido actualizda con exito!");
		response.put("especialidad", especialidadActualizada);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("especialidades/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			especialidadService.delete(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al eliminar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La especialidad ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
