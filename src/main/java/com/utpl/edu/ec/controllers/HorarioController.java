package com.utpl.edu.ec.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.utpl.edu.ec.models.entity.Horario;
import com.utpl.edu.ec.models.services.IHorarioService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class HorarioController {
	
	@Autowired
	private IHorarioService horarioService;
	
	@GetMapping("/horarios")
	public List<Horario> index() {
		return horarioService.findAll();
	}
	
	@GetMapping("/horarios/page/{page}")
	public Page<Horario> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return horarioService.findAll(pageable);
	}
	
	@GetMapping("/horarios/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Horario horario = null;
		try {
			horario = horarioService.findById(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (horario == null) {
			response.put("mensaje", "El Horario ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Horario>(horario, HttpStatus.OK);
	}
	
	
	@PostMapping("/horarios")
	public ResponseEntity<?> create(@Valid @RequestBody Horario horario, BindingResult result) {

		Horario nuevoHorario = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			nuevoHorario = horarioService.save(horario);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al insertar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El horario ha sido creado con exito!");
		response.put("horario", nuevoHorario);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/horarios/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Horario horario, BindingResult result, @PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Horario horarioActual = horarioService.findById(id);
		Horario horarioActualizado = null;
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (horarioActual == null) {
			response.put("mensaje", "Error, no se pudo editar el horario ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			horarioActual.setDias(horario.getDias());
			horarioActual.setHora_inicio(horario.getHora_inicio());
			horarioActual.setHora_fin(horario.getHora_fin());
			horarioActualizado = horarioService.save(horarioActual);

		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al actualizar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El horario ha sido actualizao con exito!");
		response.put("horario", horarioActualizado);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("horarios/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			horarioService.delete(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al eliminar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El horario ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
