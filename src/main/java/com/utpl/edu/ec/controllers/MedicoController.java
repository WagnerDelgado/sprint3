package com.utpl.edu.ec.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.utpl.edu.ec.models.entity.Medico;
import com.utpl.edu.ec.models.services.IMedicoService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class MedicoController {
	
	@Autowired
	private IMedicoService medicoService;
	
	@GetMapping("/medicos")
	public List<Medico> index() {
		return medicoService.findAll();
	}
	
	@GetMapping("/medicos/page/{page}")
	public Page<Medico> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return medicoService.findAll(pageable);
	}
	
	@GetMapping("/medicos/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Medico medico = null;
		try {
			medico = medicoService.findById(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (medico == null) {
			response.put("mensaje", "El Medico ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Medico>(medico, HttpStatus.OK);
	}
	
	
	@PostMapping("/medicos")
	public ResponseEntity<?> create(@Valid @RequestBody Medico medico, BindingResult result) {

		Medico nuevoMedico = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			nuevoMedico = medicoService.save(medico);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al insertar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El medico ha sido creado con exito!");
		response.put("medico", nuevoMedico);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/medicos/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Medico medico, BindingResult result, @PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		Medico medicoActual = medicoService.findById(id);
		Medico medicoActualizado = null;
		
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<>();
			
			for(FieldError error: result.getFieldErrors()) {
				errors.add("El campo"+ " " + error.getField() + " " + error.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (medicoActual == null) {
			response.put("mensaje", "Error, no se pudo editar el horario ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			medicoActual.setCedula(medico.getCedula());
			medicoActual.setNombres(medico.getNombres());
			medicoActual.setApellidos(medico.getApellidos());
			medicoActual.setFotoUrl(medico.getFotoUrl());
			medicoActual.setEspecialidad(medico.getEspecialidad());
			medicoActual.setHorario(medico.getHorario());		
			medicoActualizado = medicoService.save(medicoActual);

		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al actualizar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El medico ha sido actualizado con exito!");
		response.put("medicos", medicoActualizado);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	
	@DeleteMapping("medicos/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			medicoService.delete(id);
		} catch (DataAccessException ex) {
			response.put("mensaje", "Error al eliminar el registro en la base de datos");
			response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El medico ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
