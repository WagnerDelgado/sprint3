package com.utpl.edu.ec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitasBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitasBackendApplication.class, args);
	}

}
