package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.utpl.edu.ec.models.entity.Especialidad;

public interface IEspecialidadService {
	
	public List<Especialidad> findAll();
	
	public Page<Especialidad> findAll(Pageable pageable);

	public Especialidad findById(Integer id);

	public Especialidad save(Especialidad especialidad);

	public void delete(Integer id);

}
