package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.utpl.edu.ec.models.entity.Medico;

public interface IMedicoService {
	
	public List<Medico> findAll();
	
	public Page<Medico> findAll(Pageable pageable);

	public Medico findById(Integer id);

	public Medico save(Medico medico);

	public void delete(Integer id);

}
