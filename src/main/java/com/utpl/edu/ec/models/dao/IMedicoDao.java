package com.utpl.edu.ec.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.models.entity.Medico;

public interface IMedicoDao extends JpaRepository<Medico, Integer>{

}
