package com.utpl.edu.ec.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.models.entity.Especialidad;

public interface IEspecialidadDao extends JpaRepository<Especialidad, Integer>{

}
