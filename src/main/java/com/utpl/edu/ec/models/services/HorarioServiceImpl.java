package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utpl.edu.ec.models.dao.IHorarioDao;
import com.utpl.edu.ec.models.entity.Horario;
@Service
public class HorarioServiceImpl implements IHorarioService{

	@Autowired
	private IHorarioDao horarioDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Horario> findAll() {
		return (List<Horario>)  horarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Horario> findAll(Pageable pageable) {
		return horarioDao.findAll(pageable);
	}

	@Override
	@Transactional
	public Horario findById(Integer id) {
		return horarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Horario save(Horario horario) {
		return horarioDao.save(horario);
	}

	@Override
	public void delete(Integer id) {
		horarioDao.deleteById(id);			
	}
}
