package com.utpl.edu.ec.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.models.entity.Cita;

public interface ICitaDao extends JpaRepository<Cita, Integer>{

}
