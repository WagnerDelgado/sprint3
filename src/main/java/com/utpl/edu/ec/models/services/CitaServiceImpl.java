package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utpl.edu.ec.models.dao.ICitaDao;
import com.utpl.edu.ec.models.entity.Cita;

@Service
public class CitaServiceImpl implements ICitaService{
	
	@Autowired
	private ICitaDao citasDao;

	@Override
	@Transactional(readOnly = true)
	public List<Cita> findAll() {
		return (List<Cita>) citasDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Cita> findAll(Pageable pageable) {
		return citasDao.findAll(pageable);
	}

	@Override
	@Transactional
	public Cita findById(Integer id) {
		return citasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Cita save(Cita cita) {
		return citasDao.save(cita);
	}

	@Override
	@Transactional
	public void delete(Integer id) {
		citasDao.deleteById(id);
		
	}
}
