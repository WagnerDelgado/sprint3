package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.utpl.edu.ec.models.entity.Horario;

public interface IHorarioService {
	
	public List<Horario> findAll();
	
	public Page<Horario> findAll(Pageable pageable);

	public Horario findById(Integer id);

	public Horario save(Horario horario);

	public void delete(Integer id);

}
