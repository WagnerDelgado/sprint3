package com.utpl.edu.ec.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.models.entity.Paciente;

public interface IPacienteDao extends JpaRepository<Paciente, Integer>{
	
	

}
