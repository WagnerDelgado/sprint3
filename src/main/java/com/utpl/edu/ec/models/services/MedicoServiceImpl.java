package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utpl.edu.ec.models.dao.IMedicoDao;
import com.utpl.edu.ec.models.entity.Medico;

@Service
public class MedicoServiceImpl implements IMedicoService{
	
	
	@Autowired
	private IMedicoDao medicoDao;
	

	@Override
	@Transactional(readOnly = true)
	public List<Medico> findAll() {
		return (List<Medico>) medicoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Medico> findAll(Pageable pageable) {
		return medicoDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Medico findById(Integer id) {
		return medicoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Medico save(Medico medico) {
		return medicoDao.save(medico);
	}

	@Override
	@Transactional
	public void delete(Integer id) {
		medicoDao.deleteById(id);
	}
}
