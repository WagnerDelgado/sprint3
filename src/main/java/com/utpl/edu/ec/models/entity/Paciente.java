package com.utpl.edu.ec.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="pacientes")
public class Paciente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdPaciente;
	
	@Column(name = "cedula", nullable=false, length=13, unique = true)
	private String cedula;
	
	@Column(name = "nombres", nullable=false, length=100)
	private String nombres;
	
	@Column(name = "apellidos", nullable=false, length=100)
	private String apellidos;
	
	@Column(name = "sexo", nullable=false, length=100)
	private String sexo;
	
	@Column(name = "telefono", nullable=false, length=15)
	private String telefono;
	
	@Column(name = "usuario", nullable=false, length=100)
	private String usuario;
	
	@Column(name = "password", nullable=false, length=100)
	private String password;

	public Integer getIdPaciente() {
		return IdPaciente;
	}

	public void setIdPaciente(Integer idPaciente) {
		IdPaciente = idPaciente;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
