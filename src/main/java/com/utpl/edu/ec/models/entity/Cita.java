package com.utpl.edu.ec.models.entity;

import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="citas")
public class Cita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdCita;
	
	@Column(name = "fecha_cita", nullable = false)
	private LocalDateTime fecha_cita;
	
	@Column(name = "hora_cita", nullable = false, unique = true)
	private LocalTime hora_cita;
	
	@ManyToOne
	@JoinColumn(name = "IdPaciente", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_paciente"))
	private Paciente paciente;
	
	@ManyToOne
	@JoinColumn(name = "IdMedico", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_medico"))
	private Medico medico;

	public Integer getIdCita() {
		return IdCita;
	}

	public void setIdCita(Integer idCita) {
		IdCita = idCita;
	}

	public LocalDateTime getFecha_cita() {
		return fecha_cita;
	}

	public void setFecha_cita(LocalDateTime fecha_cita) {
		this.fecha_cita = fecha_cita;
	}

	public LocalTime getHora_cita() {
		return hora_cita;
	}

	public void setHora_cita(LocalTime hora_cita) {
		this.hora_cita = hora_cita;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}
}
