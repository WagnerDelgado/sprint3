package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.utpl.edu.ec.models.dao.IEspecialidadDao;
import com.utpl.edu.ec.models.entity.Especialidad;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService{
	
	@Autowired
	private IEspecialidadDao especialidadDao;

	@Override
	@Transactional(readOnly = true)
	public List<Especialidad> findAll() {
		return (List<Especialidad>) especialidadDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Especialidad> findAll(Pageable pageable) {
		return especialidadDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Especialidad findById(Integer id) {
		return especialidadDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Especialidad save(Especialidad especialidad) {
		return especialidadDao.save(especialidad);
	}

	@Override
	@Transactional
	public void delete(Integer id) {
		especialidadDao.deleteById(id);		
	}
}
