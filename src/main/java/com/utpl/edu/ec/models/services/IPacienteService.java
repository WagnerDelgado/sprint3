package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.utpl.edu.ec.models.entity.Paciente;

public interface IPacienteService {
	
	public List<Paciente> findAll();
	
	public Page<Paciente> findAll(Pageable pageable);

	public Paciente findById(Integer id);

	public Paciente save(Paciente paciente);

	public void delete(Integer id);

}
