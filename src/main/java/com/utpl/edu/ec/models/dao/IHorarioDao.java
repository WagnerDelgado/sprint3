package com.utpl.edu.ec.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.models.entity.Horario;

public interface IHorarioDao extends JpaRepository<Horario, Integer>{

}
