package com.utpl.edu.ec.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.utpl.edu.ec.models.entity.Cita;

public interface ICitaService {
	
	public List<Cita> findAll();
	
	public Page<Cita> findAll(Pageable pageable);

	public Cita findById(Integer id);

	public Cita save(Cita cita);

	public void delete(Integer id);

}
